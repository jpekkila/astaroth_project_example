## Astaroth Project Example

An example for using Astaroth as a library

## Information

There are two primary ways of linking with Astaroth.

1. Create a CMake project and add `astaroth` as a submodule

2. Build `astaroth` manually. Then, include the required Astaroth headers (`astaroth/include` and `astaroth/build/acc-runtime/api`) and link your program with the resulting `libastaroth.a`.

## Cloning and Building

    * `git clone git@bitbucket.org:jpekkila/astaroth_project_example.git && git submodule update --init --recursive`

    * `mkdir build && cd build`

    * `cmake .. && make -j`

> Note: Remember to run `git submodule update --init --recursive` to properly pull the `astaroth` subdirectory.

## Running

    * `cmake -DDSL_MODULE_DIR=../dsl/ -DBUILD_MHD_SAMPLES=OFF .. && make -j && ./myproject 32 32 7`