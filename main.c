#include <stdio.h>
#include <stdlib.h>

#include "astaroth.h"
#include "astaroth/include/astaroth.h"

static void print_mesh(const Device device, const AcMeshInfo info) {
  acDeviceSynchronizeStream(device, STREAM_ALL);

  AcMesh mesh;
  acHostMeshCreate(info, &mesh);
  acDeviceStoreMesh(device, STREAM_DEFAULT, &mesh);

  const int3 start =
      acConstructInt3Param(AC_nx_min, AC_ny_min, AC_nz_min, info);
  const int3 end = acConstructInt3Param(AC_nx_max, AC_ny_max, AC_nz_max, info);
  for (size_t w = 0; w < NUM_VTXBUF_HANDLES; ++w) {
    // for (size_t k = start.z; k < end.z; ++k) {
    for (size_t k = end.z / 2; k <= end.z / 2; ++k) {
      printf("----------\n");
      for (size_t j = start.y; j < end.y; ++j) {
        for (size_t i = start.x; i < end.x; ++i) {
          printf("%s ",
                 mesh.vertex_buffer[w][acVertexBufferIdx(i, j, k, info)] > 0.1
                     ? "x"
                     : ".");
        }
        printf("\n");
      }
      printf("----------\n");
    }
  }
  fflush(stdout);

  acHostMeshDestroy(&mesh);
}

int main(int argc, char *argv[]) {

  AcMeshInfo info;

  // Set mesh dimensions
  if (argc != 4) {
    fprintf(stderr, "Usage: ./myproject <nx> <ny> <nz>\n");
    return EXIT_FAILURE;
  } else {
    info.int_params[AC_nx] = atoi(argv[1]);
    info.int_params[AC_ny] = atoi(argv[2]);
    info.int_params[AC_nz] = atoi(argv[3]);
    acHostUpdateBuiltinParams(&info);
  }
  ERRCHK_ALWAYS(info.int_params[AC_nz] >= STENCIL_ORDER + 1);

  Device device;
  acDeviceCreate(0, info, &device);

  const int3 start =
      acConstructInt3Param(AC_nx_min, AC_ny_min, AC_nz_min, info);
  const int3 end = acConstructInt3Param(AC_nx_max, AC_ny_max, AC_nz_max, info);
  acDeviceLaunchKernel(device, STREAM_DEFAULT, init, start, end);
  acDeviceSwapBuffers(device);

  const int3 mm_min = (int3){0, 0, 0};
  const int3 mm_max = (int3){
      info.int_params[AC_mx],
      info.int_params[AC_my],
      info.int_params[AC_mz],
  };
  acDevicePeriodicBoundconds(device, STREAM_DEFAULT, mm_min, mm_max);
  print_mesh(device, info);

  for (size_t i = 0; i < 10000; ++i) {
    acDeviceLaunchKernel(device, STREAM_DEFAULT, step, start, end);
    acDeviceSwapBuffers(device);
    acDevicePeriodicBoundconds(device, STREAM_DEFAULT, mm_min, mm_max);
    print_mesh(device, info);
    usleep(100000);
  }

  acDeviceDestroy(device);
  return EXIT_SUCCESS;
}